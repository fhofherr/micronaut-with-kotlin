# Micronaut with Kotlin

Micronaut is a relatively new JVM based framework. It promises faster
startup times and a lower memory footprint. Additionally it can be used
to develop server-less functions. Micronaut applicatons can be written
using Java, Groovy, or Kotlin. This talk demonstrates the core features
of Micronaut with Kotlin using a sample application and compares them
with a similar Spring application.
