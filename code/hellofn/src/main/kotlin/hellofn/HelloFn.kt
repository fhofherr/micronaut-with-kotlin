package hellofn

import io.micronaut.function.FunctionBean
import java.util.function.Function

@FunctionBean("hellofn")
class HelloFn(
    private val greeter: Greeter
) : Function<GreetingRequest, String> {

    override fun apply(req: GreetingRequest): String = req
        .username
        ?.let {
            greeter.greet(it)
        } ?: greeter.greet()
}

data class GreetingRequest(val username: String?)
