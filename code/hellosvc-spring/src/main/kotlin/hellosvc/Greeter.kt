package hellosvc

import org.springframework.stereotype.Component

@Component
class Greeter {

    fun greet(): String = "Hello!"

    fun greet(userName: String): String = "Hello, $userName!"
}
