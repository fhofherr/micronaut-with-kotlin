package hellosvc

import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController("/greeting")
class GreetingController(
    private val greeter: Greeter
) {
    @GetMapping(
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun index(@RequestParam("userName") userName: String? = null) = userName?.let {
        GreetingResponse(greeter.greet(it))
    } ?: GreetingResponse(greeter.greet())

    data class GreetingResponse(val greeting: String)
}
