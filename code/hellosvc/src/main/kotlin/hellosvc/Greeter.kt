package hellosvc

import javax.inject.Singleton

@Singleton
class Greeter {

    fun greet(): String = "Hello!"

    fun greet(userName: String): String = "Hello, $userName!"
}
