package hellosvc

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import javax.annotation.Nullable

@Controller("/greeting")
class GreetingController(
    private val greeter: Greeter
) {
    @Get(
        value = "/{?userName}",
        produces = [MediaType.APPLICATION_JSON]
    )
    fun index(@Nullable userName: String?) = userName?.let {
        GreetingResponse(greeter.greet(it))
    } ?: GreetingResponse(greeter.greet())

    data class GreetingResponse(val greeting: String)
}
