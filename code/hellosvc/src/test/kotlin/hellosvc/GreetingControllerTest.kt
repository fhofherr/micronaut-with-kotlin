package hellosvc

import hellosvc.GreetingController.GreetingResponse
import io.micronaut.context.ApplicationContext
import io.micronaut.http.client.HttpClient
import io.micronaut.runtime.server.EmbeddedServer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class GreetingControllerTest {

    private var server = ApplicationContext.run(EmbeddedServer::class.java)
    private var client = HttpClient.create(server.url)

    @AfterEach
    internal fun tearDown() {
        client.stop()
        server.stop()
    }

    @Test
    internal fun `get plain greeting`() {
        val expected = GreetingResponse("Hello!")
        val actual = client.toBlocking().retrieve("/greeting", GreetingResponse::class.java)
        assertEquals(expected, actual)
    }

    @Test
    internal fun `get customized greeting`() {
        val userName = "John"
        val expected = GreetingResponse("Hello, $userName!")
        val actual = client
            .toBlocking()
            .retrieve(
                "/greeting?userName=$userName",
                GreetingResponse::class.java
            )
        assertEquals(expected, actual)
    }
}
