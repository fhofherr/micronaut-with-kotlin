1. What is Micronaut
    1. Quick history of the project
    2. Core features
       * User Guide provides many samples
    3. Goals
2. Sample Service
    1. Service features
    2. Project scaffolding: `mn create-app hello-service  --lang=kotlin --features=management,tracing-jaeger` command
    3. Dependency Injection => `greeter` component
3. Lambda Functions
    1. Project scaffolding: `mn` command
    2. Executing functions locally
3. Comparison with Spring application
    1. Start-up time
    2. Fat-Jar size
4. Wrap-up
