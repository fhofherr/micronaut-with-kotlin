<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>Micronaut with Kotlin</title>

    <link rel="stylesheet" href="css/reveal.css">
    <link rel="stylesheet" href="css/theme/black.css">

    <!-- Theme used for syntax highlighting of code -->
    <link rel="stylesheet" href="lib/css/zenburn.css">

    <!-- Printing and PDF exports -->
    <script>
      var link = document.createElement( 'link' );
      link.rel = 'stylesheet';
      link.type = 'text/css';
      link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
      document.getElementsByTagName( 'head' )[0].appendChild( link );
    </script>
  </head>
  <body>
    <div class="reveal">
      <div class="slides">
        <section>
          <h1>Micronaut with Kotlin</h1>
          <p>Ferdinand Hofherr</p>
          <p>Exaring AG</p>
        </section>

        <section>
          <section>
          <h1>Micronaut</h1>
          </section>

          <section>
            <h2>History</h2>
            <ul>
              <li><strong>March 2017</strong>: initial commit to <a href="https://github.com/micronaut-projects/micronaut-core"><code>micronaut-core</code></a></li>
              <li><strong>May 2018</strong>: <a href="https://objectcomputing.com/news/2018/05/23/micronaut-open-sourced">open sourced</a></li>
              <li><strong>October 2018</strong>: <a href="https://objectcomputing.com/news/2018/10/23/micronaut-10-ga-released">Version 1.0 released</a></li>
            </ul>
          </section>

          <section>
            <h2>Selling points</h2>
            <ul>
              <li>Faster and nearly constant startup time</li>
              <li>Lower memory consumption</li>
              <li>Familiar to Spring developers</li>
              <li>Supports Kotlin, Java and Groovy</li>
              <li>Built-in Cloud support:<ul>
                <li>Distributed tracing</li>
                <li>Service discovery</li>
                <li>Serverless functions</li>
              </ul></li>
            </ul>
            <aside class="notes">
              <ul>
                <li>At least for me :-)</li>
                <li>Minimizes the use of reflection and dynamic proxies<ul>
                    <li>Startup time does not depend on size of your code base</li>
                  </ul>
                </li>
                <li>Annotation processors generate code at compile time</li>
                <li>No need to search for libraries which add cloud features to your framework</li>
              </ul>
            </aside>
          </section>

          <section>
            <h2>Micronaut@Exaring</h2>
            Currently for two AWS Lambda functions
            <aside class="notes">
              <ul>
                <li>MN Lambdas were developed in mid-2018</li>
                <li>Most of our services are in Spring already</li>
                <li>Did not try MN for services yet</li>
                <li>I'm not going to recommend one over the other</li>
              </ul>
            </aside>
          </section>
        </section>

        <section>
          <section>
            <h1>Service example</h1>
          </section>

          <section>
            <h2>Features demonstrated</h2>
            <ul>
              <li>Project scaffolding</li>
              <li>Dependency Injection</li>
              <li>HTTP server</li>
            </ul>
          </section>

          <section>
            <h2>Project scaffolding</h2>
            <pre><code class="bash" data-trim>
            mn create-app hello-service --lang=kotlin
            </code></pre>
            <pre><code data-trim>
            hellosvc
            ├── Dockerfile
            ├── build.gradle
            ├── gradle*
            ├── micronaut-cli.yml
            └── src
                ├── main
                │   ├── kotlin
                │   │   └── ...
                │   └── resources
                │       ├── application.yml
                │       └── logback.xml
                └── test
                    └── ...
            </code></pre>
            <aside class="notes">
              <ul>
                <li>Kotlin version is 1.2.61</li>
                <li>Replacing it with 1.3.11 works :-)</li>
                <li><code>gradle build</code> creates a fat jar</li>
              </ul>
            </aside>
          </section>

          <section>
            <h2>IOC</h2>
            <ul>
              <li>Micronaut implements JSR-330</li>
              <li>Just use the annotations in <code>javax.inject</code></li>
              <li>Micronaut generates code at <strong>compile time</strong></li>
            </ul>
          </section>

          <section>
            <h2>Goals of Micronaut IOC</h2>
            <blockquote cite="https://docs.micronaut.io/latest/guide/index.html#ioc">
              <ul>
                <li>Use reflection as a last resort</li>
                <li>Avoid proxies</li>
                <li>Optimize start-up time</li>
                <li>Reduce memory footprint</li>
                <li>Provide clear, understandable error handling</li>
              </ul>
              <footer>
                <cite>
                  <a href="https://docs.micronaut.io/latest/guide/index.html#ioc">Micronaut User Guide</a>
                </cite>
              </footer>
            </blockquote>
          </section>

          <section>
            <h2>How does it work?</h2>
            <ul>
              <ul>
                <li>Java/Kotlin<ul>
                    <li>Annotation Processor + Byte Code generation at compile time</li>
                  </ul>
                </li>
                <li>Groovy<ul>
                 <li>AST transformation</li>
                </ul></li>
              </ul>
            </ul>
            <aside class="notes">
              <ul>
                <li>Benefits:<ul>
                    <li>no reflection =&gt; JVM can optimize </li>
                    <li>constant and faster startup time</li>
                    <li>no need to cache data needed for reflection based code generation</li>
                </ul></li>
              </ul>
            </aside>
          </section>

          <section>
            <h2><code>Greeter</code></h2>
            <pre><code class="kotlin" data-trim>
            import javax.inject.Singleton

            @Singleton
            class Greeter {

                fun greet(): String = "Hello!"

                fun greet(userName: String): String = "Hello, $userName!"
            }
            </code></pre>
            <aside class="notes">
             <ul>
               <li><code>@Singleton</code> annotation defines a Singleton Component</li>
               <li>Similar to <code>@Component</code> in Spring</li>
               <li>The rest is just plain Kotlin</li>
             </ul>
            </aside>
          </section>

          <section>
            <h2><code>GreetingController</code></h2>
            <pre><code class="kotlin" data-trim>
            @Controller("/greeting")
            class GreetingController(
                private val greeter: Greeter
            ) {
                @Get(
                    value = "/{?userName}",
                    produces = [MediaType.APPLICATION_JSON]
                )
                fun index(@Nullable userName: String?) = userName?.let {
                    GreetingResponse(greeter.greet(it))
                } ?: GreetingResponse(greeter.greet())

                data class GreetingResponse(val greeting: String)
            }
            </code></pre>
            <aside class="notes">
              <ul>
                <li><code>@Controller</code> defines an HTTP Controller</li>
                <li><code>@Controller</code> is Micronaut specific</li>
                <li><code>@Get</code> defines a GET resource</li>
                <li><code>@Nullable</code>Needed by Micronaut annotation processor</li>
              </ul>
            </aside>
          </section>

          <section>
            <h2>Test - EmbeddedServer and HttpClient</h2>
            <pre><code class="kotlin" data-trim>
            internal class GreetingControllerTest {

                private var server = ApplicationContext.run(
                    EmbeddedServer::class.java
                )
                private var client = HttpClient.create(server.url)

                @AfterEach
                internal fun tearDown() {
                    client.stop()
                    server.stop()
                }
            }
            </code></pre>
          </section>

          <section>
            <h2>Test - retrieving resources</h2>
            <pre><code class="kotlin" data-trim>
            @Test
            internal fun `get customized greeting`() {
                val userName = "John"
                val expected = GreetingResponse("Hello, $userName!")
                val actual = client
                    .toBlocking()
                    .retrieve(
                        "/greeting?userName=$userName",
                        GreetingResponse::class.java
                    )
                assertEquals(expected, actual)
            }
            </code></pre>
          </section>

          <section>
            <h2>Application.kt</h2>
            <pre><code class="kotlin" data-noescape data-trim>
            object Application {

                @JvmStatic
                fun main(args: Array<String>) {
                    Micronaut.build()
                        .packages("hellosvc")
                        .mainClass(Application.javaClass)
                        .start()
                }
            }
            </code></pre>
          </section>
        </section>

        <section>
          <section>
            <h1>Serverless Function</h1>
          </section>
          <section>
            <h2>Scaffolding</h2>
            <pre><code class="bash" data-trim>
            mn create-function --lang kotlin hellofn --test junit
            </code></pre>
            <ul>
              <li>creates structure similar to service example</li>
              <li>requires minor tweaks:<ul>
                <li>bug in shadowJar plugin: downgrade to 2.0.0</li>
                <li>example tests in JUnit 4 and Java</li>
              </ul></li>
            </ul>
          </section>

          <section>
            <h2><code>build.gradle</code></h2>
            <pre><code class="groovy" data-trim>
            ...
            mainClassName = "io.micronaut.function.executor.FunctionApplication"
            ...
            dependencies {
            ...
            compile "io.micronaut:micronaut-function-aws"
            ...
            }
            </code></pre>
          </section>

          <section>
            <h2>Function selection</h2>
            <p><code>application.yml</code>:</p>
            <pre><code class="groovy" data-trim>
            micronaut:
              function:
                name: hellofn
            </code></pre>
            <p>or environment variable:</p>
            <pre><code class="bash" data-trim>
            export MICRONAUT_FUNCTION_NAME="hellofn"
            </code></pre>
            <aside class="notes">
              <ul>
                <li>A single JAR can contain multiple functions</li>
                <li>Environment variable enables selection upon deployment</li>
              </ul>
            </aside>
          </section>

          <section>
            <h2><code>HelloFn</code></h2>
            <pre><code class="kotlin" data-noescape data-trim>
            import io.micronaut.function.FunctionBean
            import java.util.function.Function

            @FunctionBean("hellofn")
            class HelloFn(
                private val greeter: Greeter
            ) : Function&lt;GreetingRequest, String&gt; {

                override fun apply(req: GreetingRequest): String = req
                    .username?.let { greeter.greet(it) }
                    ?: greeter.greet()
            }

            data class GreetingRequest(val username: String?)
            </code></pre>
            <aside class="notes">
            <ul>
              <li><code>@FunctionBean</code> marks class as function and gives it a name</li>
              <li>class has to implement one of the interfaces of <code>java.util.function.*</code></li>
            </ul>
            </aside>
          </section>

          <section>
            <h2>Execute on command line</h2>
            <pre><code class="bash" data-trim>
            ./gradlew clean build

            echo '{"username": null}' | java -jar build/libs/hellofn-0.1-all.jar
            Hello!

            echo '{"username": "John"}' | java -jar build/libs/hellofn-0.1-all.jar
            Hello, John!
            </code></pre>
          </section>
          <aside class="notes">
            <ul>
              <li>Functions can also be started as a service</li>
            </ul>
          </aside>
        </section>

        <section>
          <section>
            <h1>Conclusion</h1>
          </section>

          <section>
            <h2>Startup Time</h2>
            <table>
              <tr>
                <th>MN Service</th>
                <th>Spring Service</th>
                <th>MN Lambda</th>
              </tr>
              <tr>
                <td>~10s</td>
                <td>~21s</td>
                <td>~3s</td>
              </tr>
            </table>
            <aside class="notes">
              <ul>
                <li>Micronaut startup time is faster than Spring</li>
                <li>Both are not great</li>
                <li>Lambda startup time is surprising (but still not great)</li>
                <li>Sub-second startup time would be great</li>
              </ul>
            </aside>
          </section>

          <section>
            <h2>Micronaut or Spring?</h2>
            <ul>
              <li>Both are good frameworks</li>
              <li>Micronaut:<ul>
                  <li>New and fresh ideas</li>
                  <li>Built-in cloud support</li>
                  <li>Fun to write</li>
              </ul></li>
              <li>Spring:<ul>
                <li>Battle proven</li>
                <li>Mature</li>
                <li>Solution for most problems (if you know where to look)</li>
              </ul></li>
            </ul>
          </section>
        </section>

        <section>
          <h1>Thank you</h1>
        </section>
      </div>

    <script src="lib/js/head.min.js"></script>
    <script src="js/reveal.js"></script>

    <script>
      // More info about config & dependencies:
      // - https://github.com/hakimel/reveal.js#configuration
      // - https://github.com/hakimel/reveal.js#dependencies
      Reveal.initialize({
        dependencies: [
          { src: 'plugin/markdown/marked.js' },
          { src: 'plugin/markdown/markdown.js' },
          { src: 'plugin/notes/notes.js', async: true },
          { src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } }
        ]
      });
    </script>
  </body>
</html>
<!-- vim: set spell spelllang=en: -->
